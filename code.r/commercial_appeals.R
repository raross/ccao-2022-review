
library(here)
source(here::here('ccao-2022-review', 'code.r', '0_utilities.r'))
plots <- list()

# ---- Sales ratios ----

# Data is downloaded from Cook County Open Data and saved by code.r/0_ingest_ccao_open_data.R
sales <- read_parquet(here::here('ccao-2022-review', 'data', 'Cook_County_Assessor_s_Sales.parquet')) %>%
          dplyr::mutate(Year = factor(lubridate::year(sale_date)))

n_sales <- read_parquet(here::here('ccao-2022-review', 'data', 'Cook_County_Assessor_s_Assessments.parquet')) %>%
  # Keep only commercial properties
  dplyr::filter(between(Class,300, 399) | between(Class, 500, 599)) %>%
  dplyr::mutate(`Property type` = case_when( Class <399 ~ 'Large Multi-Family Apartments', TRUE ~ 'Commercial')
                , Year=  factor(Year, levels = 2010:2020)
                , triad = ccao::town_get_triad(as.character(Township.code))
  ) %>%
  inner_join(sales) %>%
  group_by(Year,valid_sale, `Property type`, triad) %>% summarise(n = n())

ratios <- read_parquet(here::here('ccao-2022-review', 'data', 'Cook_County_Assessor_s_Assessments.parquet')) %>%
  # Keep only commercial properties
  dplyr::filter(between(Class,300, 399) | between(Class, 500, 599)) %>%
  # Keep only assessments from re-assessment years
  dplyr::mutate(triad = ccao::town_get_triad(as.character(Township.code))) %>%
  dplyr::filter(
    (triad == 1 & Year ==2021) |
      (triad == 1 & Year ==2018) |
      (triad == 1 & Year ==2015) |
      (triad == 1 & Year ==2012) |
      (triad == 2 & Year ==2019) |
      (triad == 2 & Year ==2016) |
      (triad == 2 & Year ==2013) |
      (triad == 2 & Year ==2010) |
      (triad == 3 & Year ==2020) |
      (triad == 3 & Year ==2017) |
      (triad == 3 & Year ==2014) |
      (triad == 3 & Year ==2011) 
  ) %>%
  # Change logicals to logical type
  dplyr::mutate(CCAO.appealed = CCAO.appealed == 'true'
      , CCAO.changed = (CCAO.certified < CCAO.mailed)
      , BOR.appealed = BOR.appealed == 'true'
      , BOR.changed = BOR.changed == 'true'
  ) %>%
  dplyr::mutate(
      triad = case_when (
        triad ==  1 ~ 'City'
        , triad == 2 ~ 'NW Suburbs'
        , triad == 3 ~ 'SW Suburbs')
      , `Administration` = case_when(
         Year >= 2019 ~ 'Fritz Kaegi (2019 - 2020)'
      , between(Year, 2011, 2018) ~ 'Joseph Berrios (2011 - 2018)'
      , TRUE ~ 'James Houlihan')
      , Year=  factor(Year, levels = 2010:2020)
  ) %>%
  inner_join(sales) %>%
  dplyr::filter(valid_sale == 1) %>%
  group_by(doc_no, Year, triad, Administration)  %>%
  dplyr::summarise(
    PINs = n()
    , `Min class` = min(Class)
    , `Max class` = max(Class)
    , `Sale price` = max(as.numeric(sale_price))
    , `Total ccao mailed AV` = sum(CCAO.mailed)
    , `Total ccao certified AV` = sum(CCAO.certified)
    , `Total bor certified AV` = sum(BOR.certified)
    , `CCAO appeal` = max(CCAO.appealed)
    , `CCAO appeal win` = max(CCAO.changed) 
    , `BOR appeal` = max(BOR.appealed) 
    , `CCAO appeal win` = max(BOR.changed) 
  ) %>%
  dplyr::mutate(
    `Mailed ratio` = `Total ccao mailed AV` / `Sale price`
    , `CCAO certified ratio` = `Total ccao certified AV` / `Sale price`
    , `BOR certified ratio` = `Total bor certified AV` / `Sale price`
  ) %>%
  dplyr::mutate(`Property type` = case_when( `Min class` <399 ~ 'Large Multi-Family Apartments', TRUE ~ 'Commercial')) 

# remove unneeded files
rm(sales)

# Sample sizes
ratios %>%
  dplyr::group_by(`Property type`, Year ) %>%
  dplyr::summarise(n = n())

# Ratios by administration
graph_data <- ratios %>%
  ungroup() %>%
  dplyr::select(Year, `Mailed ratio`, `CCAO certified ratio`,`BOR certified ratio`, `Property type`, `Sale price`, doc_no) %>%
  dplyr::rename(`CCAO Initial Values` = `Mailed ratio`
                , `CCAO Certified Values` = `CCAO certified ratio`
                , `BOR Certified Values` = `BOR certified ratio`) %>%
  pivot_longer(!Year & !`Property type` & !`Sale price` &! doc_no, names_to = "Assessment stage", values_to = "Sales ratio") 

graph_data <- ratios %>%
  ungroup() %>%
  dplyr::select(Year, `Property type`, `Total ccao mailed AV`, `Total ccao certified AV`,`Total bor certified AV`, `Sale price`, doc_no) %>%
  dplyr::rename(`CCAO Initial Values` = `Total ccao mailed AV`
                , `CCAO Certified Values` = `Total ccao certified AV`
                , `BOR Certified Values` = `Total bor certified AV`) %>%
  pivot_longer(!doc_no & !Year & !`Property type` & !`Sale price`, names_to = "Assessment stage", values_to = "Assessment") %>%
  inner_join(graph_data)

graph_data$`Assessment stage` <- 
   factor(graph_data$`Assessment stage`, levels = 
        c( 'CCAO Initial Values', 'CCAO Certified Values','BOR Certified Values'))
  
plots$Figure1.1 <- class_300_ratios <- graph_data %>%
  dplyr::filter(`Property type` == 'Large Multi-Family Apartments') %>%
  ggplot(aes(x=Year, y=`Sales ratio`, fill = `Assessment stage`)) + 
  geom_boxplot(outlier.shape = NA) +
  scale_y_continuous(limits=c(0,.3),labels = scales::percent_format()) +
  theme_minimal() + geom_hline(yintercept = .1, size =2, linetype ='dashed') +
  theme(legend.position = 'bottom') +
  scale_color_manual(values = c(ccao_colors$navy, ccao_colors$gold, ccao_colors$lightgreen)) +
  labs(title = "Large Mult-Family Apartments"
       , caption = 'Data from the Cook County Open Data Portal.')

plots$Figure1.2 <- class_500_ratios <- graph_data %>%
  dplyr::filter(`Property type` == 'Commercial') %>%
  ggplot(aes(x=Year, y=`Sales ratio`, fill = `Assessment stage`)) + 
  geom_boxplot(outlier.shape = NA) +
  scale_y_continuous(limits=c(0,.5),labels = scales::percent_format()) +
  theme_minimal() + geom_hline(yintercept = .25, size =2, linetype ='dashed') +
  theme(legend.position = 'bottom') +
  labs(title = "Commercial Property"
       , caption = 'Data from the Cook County Open Data Portal.')


plots$Figure1 <- ggarrange(class_300_ratios, class_500_ratios)



# Sales ratio stats
ratio_stats <- graph_data %>%
  group_by(Year, `Property type`,  `Assessment stage`) %>%
  dplyr::summarise(
    COD = assessr::cod(`Sales ratio`)
    , PRD = assessr::prd(Assessment, `Sale price`)
  )

# ---- Appeals ----

appeals <- read_parquet(here::here('ccao-2022-review', 'data', 'Cook_County_Assessor_s_Assessments.parquet')) %>%
  # Keep only commercial properties
  dplyr::filter(between(Class,300, 398) | between(Class, 500, 598)) %>%
  # Keep only assessments from re-assessment years
  dplyr::mutate(triad = ccao::town_get_triad(as.character(Township.code))) %>%
  dplyr::filter(
    (triad == 1 & Year ==2021) |
      (triad == 1 & Year ==2018) |
      (triad == 1 & Year ==2015) |
      (triad == 1 & Year ==2012) |
      (triad == 2 & Year ==2019) |
      (triad == 2 & Year ==2016) |
      (triad == 2 & Year ==2013) |
      (triad == 2 & Year ==2010) |
      (triad == 3 & Year ==2020) |
      (triad == 3 & Year ==2017) |
      (triad == 3 & Year ==2014) |
      (triad == 3 & Year ==2011) 
  ) %>%
  # Change logicals to logical type
  dplyr::mutate(CCAO.appealed = CCAO.appealed == 'true'
                , CCAO.changed = CCAO.changed == 'true'
                , BOR.appealed = BOR.appealed == 'true'
                , BOR.changed = BOR.changed == 'true'
  ) %>%
  dplyr::mutate(
    triad = case_when (
      triad ==  1 ~ 'City'
      , triad == 2 ~ 'NW Suburbs'
      , triad == 3 ~ 'SW Suburbs')
    , `Administration` = case_when(
      Year >= 2019 ~ 'Fritz Kaegi (2019 - 2020)'
      , between(Year, 2011, 2018) ~ 'Joseph Berrios (2011 - 2018)'
      , TRUE ~ 'James Houlihan')
    , Year=  factor(Year, levels = 2010:2020)
    , `Property type` = case_when( Class <399 ~ 'Large Multi-Family Apartments', TRUE ~ 'Commercial')
    , CCAO.reduction = case_when(CCAO.appealed & CCAO.changed  ~
      (CCAO.certified - CCAO.mailed) / CCAO.mailed)
    , BOR.reduction = case_when(BOR.appealed & BOR.changed ~
      (BOR.certified - CCAO.certified) / CCAO.certified)
  ) %>%
  group_by(triad, Year, `Property type`) 

appeals1 <- appeals %>%
  dplyr::summarise(
    `CCAO appeal rate` = mean(CCAO.appealed)
    , `BOR appeal rate` = mean(BOR.appealed)
  )
appeals2 <- appeals %>%
  dplyr::filter(CCAO.appealed) %>%
  dplyr::summarise(
    `CCAO appeal success rate` = mean(CCAO.changed)
    , `Median CCAO reduction` = median(CCAO.reduction, na.rm = TRUE)
  )
appeals3 <- appeals %>%
  dplyr::filter(BOR.appealed) %>%
  dplyr::summarise(
    `BOR appeal success rate` = mean(BOR.changed)
    , `Median BOR reduction` = median(BOR.reduction, na.rm = TRUE)
  )

appeals <- left_join(appeals1, appeals2) %>% left_join(appeals3) %>% arrange(Year)

rm(appeals1, appeals2, appeals3)
# Graph reductions

plots$Figure2.1 <- appeals1 <- appeals %>%
  ggplot(aes( y = `CCAO appeal success rate`, x = Year)) +
  geom_bar(stat = 'identity') +
  theme_minimal() +
  scale_y_continuous(labels = scales::percent_format()) +
  labs(title = 'Success rates of commercial CCAO appeals') +
  facet_grid(rows = vars(`Property type`))

plots$Figure2.2 <- appeals2 <- appeals %>%
  ggplot(aes( y = `Median CCAO reduction`, x = Year)) +
  geom_bar(stat = 'identity') +
  theme_minimal() +
  scale_y_continuous(labels = scales::percent_format()) +
  labs(title = 'Percent reduction for successful CCAO appeals') +
  facet_grid(rows = vars(`Property type`))

plots$Figure2 <- ggarrange(appeals1, appeals2)
# ---- Individual examples ----

examples <- ratios %>%
  group_by(Year, `Property type`) %>%
  dplyr::mutate(ratio_tile = ntile(`Mailed ratio`, 5)) %>%
  dplyr::filter(ratio_tile == 5 & Year == 2020 & `CCAO appeal`) %>%
  dplyr::select(doc_no, `Min class`, `Max class`, `Sale price`
                , `Total ccao mailed AV`, `Mailed ratio`,`Total ccao certified AV`
                , `CCAO appeal`, `CCAO appeal win`)


sales %>% dplyr::filter(PIN == '24-35-200-013-0000')


# ---- Save plots ----
save(plots, file = here::here('ccao-2022-review','reports', 'plots.Rda'))